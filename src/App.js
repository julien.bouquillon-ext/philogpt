import React, { useState, useRef, useEffect, useCallback } from "react";
import "./App.css";
import ChatBox from "./components/Chatbox";
import chatData from './json/chatPrompts.json';
import { getStimulation } from "./consts/prompts";
import ChatSearch from "./components/ChatSearch";
import InfosPhilosophe from "./components/InfosPhilosophe";
import { sendMessageToApi } from "./components/apiHandler";

import img0 from "./profilepics/small/0.png"
import img1 from "./profilepics/small/1.png"
import img2 from "./profilepics/small/2.png";
import img3 from "./profilepics/small/3.png";
import img4 from "./profilepics/small/4.png";
import img5 from "./profilepics/small/5.png";
import img6 from "./profilepics/small/6.png";
import img7 from "./profilepics/small/7.png";
import img8 from "./profilepics/small/8.png";
import img9 from "./profilepics/small/9.png";
import img10 from "./profilepics/small/10.png";
import img11 from "./profilepics/small/11.png";
import img12 from "./profilepics/small/12.png";
import img13 from "./profilepics/small/13.png";
import img14 from "./profilepics/small/14.png";
import img15 from "./profilepics/small/15.png";
import img16 from "./profilepics/small/16.png";
import img17 from "./profilepics/small/17.png";
import img18 from "./profilepics/small/18.png";
import img19 from "./profilepics/small/19.png";
import img20 from "./profilepics/small/20.png";
import img21 from "./profilepics/small/21.png";
import img22 from "./profilepics/small/22.png";
import img23 from "./profilepics/small/23.png";
import img24 from "./profilepics/small/24.png";
import img25 from "./profilepics/small/25.png";

const smallImages = [
    img0, img1, img2, img3, img4, img5, img6, img7, img8, img9,
    img10, img11, img12, img13, img14, img15, img16, img17, img18, img19,
    img20, img21, img22, img23, img24, img25
];

function App() {
  const [inputUser, setInputUserState] = useState({});
  const [id, setId] = useState(0);
  const timeoutIdRef = useRef(null);
  const [philosopher, setPhilosopher] = useState(null);
  const [dataChat, setDataChat] = useState([]);
  const [filtreEpoque, setFiltreEpoque] = useState(null);
  const [searchText, setSearchText] = useState("");
  const [showInfos, setShowInfos] = useState(false);
  const [isSidebarVisible, setSidebarVisible] = useState(true);
  const [addGroupMode, setAddGroupMode] = useState(false);
  const [idShowInfosWhenGroup, setIdShowInfosWhenGroup] = useState(0);
  const [latestId, setLatestId] = useState(0);
  const [unreadMessages, setUnreadMessages] = useState(0);
  const [oldMessages, setOldMessages] = useState([]);

  const [mustSaveData, setMustSaveData] = useState(false);

  const unreadMessagesRef = useRef(unreadMessages);
  const intervalIdRef = useRef(null); // Ref pour gérer l'identifiant de l'intervalle

  useEffect(() => {
    unreadMessagesRef.current = unreadMessages;
  }, [unreadMessages]);

  

  const nouvelleIntervention = useCallback(async () => { // fonction pour qu'un philosophe "au hasard" (en fait, l'un des 6 les moins récents) fasse une intervention spontanée
    const currentUnreadMessages = unreadMessagesRef.current;
    //console.log("unreadMessages", currentUnreadMessages);
  
    if (currentUnreadMessages < 2) {
      const nonGroupPhilosophers = dataChat.filter(
        (philosopher) => philosopher.categorie !== "Groupe"
      );
  
      const sortedPhilosophers = nonGroupPhilosophers.sort((a, b) => {
        const lastMessageDateA = new Date(a.messages[a.messages.length - 1].date);
        const lastMessageDateB = new Date(b.messages[b.messages.length - 1].date);
        return lastMessageDateA - lastMessageDateB;
      });

      //console.log("sortedPhilosophers", sortedPhilosophers);
  
      const selectedPhilosophers = sortedPhilosophers.slice(0, 5);

      //console.log("selectedPhilosophers", selectedPhilosophers);


      const randomPhilosopher = selectedPhilosophers[Math.floor(Math.random() * selectedPhilosophers.length)];
  
      if (randomPhilosopher) {

  
        const prompt = getStimulation(randomPhilosopher.nom, randomPhilosopher.prompt, randomPhilosopher.markdown);
  
        let newMessages = [...randomPhilosopher.messages, { role: "assistant", content: "", date: new Date().toISOString() }];
  
        setDataChat((prevDataChat) =>
          prevDataChat.map((philosopher) =>
            philosopher.id === randomPhilosopher.id
              ? { ...philosopher, messages: newMessages, unread: true }
              : philosopher
          )
        );
  
        await sendMessageToApi(
          [],
          (partialResponse) => {
            newMessages = newMessages.map((msg, index) => {
              if (index === newMessages.length - 1) {
                return {
                  ...msg,
                  content: partialResponse, // Remplace le contenu par la nouvelle réponse partielle
                };
              }
              return msg;
            });
  
            setDataChat((prevDataChat) =>
              prevDataChat.map((philosopher) =>
                philosopher.id === randomPhilosopher.id
                  ? { ...philosopher, messages: newMessages }
                  : philosopher
              )
            );
          },
          () => {},  // Fonction vide pour setThinking
          () => {},  // Fonction vide pour setGenerating
          prompt,
          '', // Pas de prompt spécialisé requis
        );
        setMustSaveData(true);
      }
    }
  }, [dataChat]);

  useEffect(() => {
    const intervalDuration = unreadMessages === 0 ? 12000 : 25000;
  
    intervalIdRef.current = setInterval(() => {
      nouvelleIntervention();
    }, intervalDuration);
  
    return () => {
      clearInterval(intervalIdRef.current);
    };
  }, [nouvelleIntervention, unreadMessages]);
  
  
  useEffect(() => {
    // Met à jour le titre de la page en fonction du nombre de messages non lus
    if (unreadMessages === 0) {
      document.title = "PhiloGPT";
    } else {
      document.title = `(${unreadMessages}) PhiloGPT`;
    }
  }, [unreadMessages]);
  

  useEffect(() => {
    const savedDataChat = localStorage.getItem('dataChat');
  
    if (savedDataChat) {
      const parsedDataChat = JSON.parse(savedDataChat);
  
      // Trouver l'ID maximum dans parsedDataChat pour gérer les nouveaux IDs
      let maxId = parsedDataChat.reduce((max, item) => Math.max(max, item.id), 0);
  
      // Tableau temporaire pour stocker les philosophes mis à jour
      let updatedDataChat = [...parsedDataChat];
  
      // Boucler sur chatData pour trouver les nouveaux philosophes
      chatData.forEach((item) => {
        const savedItem = parsedDataChat.find((saved) => saved.id === item.id);
  
        if (!savedItem) {
          // Si le philosophe n'existe pas dans parsedDataChat, l'ajouter
          updatedDataChat.push({
            ...item,
            messages: [{ role: "assistant", content: item.greeting, date: new Date().toISOString() }],
            unread: false,
          });
        } else if (savedItem.categorie === "Groupe") {
          // Si un philosophe de la catégorie "Groupe" a le même ID qu'un nouveau philosophe, donner un nouvel ID au groupe
          const newId = ++maxId;
          updatedDataChat = updatedDataChat.map((philosopher) =>
            philosopher.id === savedItem.id && philosopher.categorie === "Groupe"
              ? { ...philosopher, id: newId } // Assigner un nouvel ID au philosophe "Groupe"
              : philosopher
          );
  
          // Ajouter le nouveau philosophe à la place de celui d'origine
          updatedDataChat.push({
            ...item,
            messages: [{ role: "assistant", content: item.greeting, date: new Date().toISOString() }],
            unread: false,
          });
        } else {
          // Si un philosophe avec le même ID est déjà dans parsedDataChat (non-Groupe), garder ses messages et son état
          updatedDataChat = updatedDataChat.map((philosopher) =>
            philosopher.id === item.id
              ? {
                  ...item,
                  messages: savedItem.messages || [{ role: "assistant", content: item.greeting, date: new Date().toISOString() }],
                  unread: savedItem.unread || false,
                }
              : philosopher
          );
        }
      });
  
      // S'assurer que tous les philosophes du parsedDataChat sont intégrés
      parsedDataChat.forEach((savedItem) => {
        const existingItem = updatedDataChat.find((item) => item.id === savedItem.id);
        if (!existingItem) {
          updatedDataChat.push(savedItem);
        }
      });
  
      // Sauvegarder les données mises à jour dans le localStorage
      //localStorage.setItem('dataChat', JSON.stringify(updatedDataChat));
      setDataChat(updatedDataChat);
      setMustSaveData(true);
  
      // Mettre à jour l'ID du dernier philosophe actif
      let latestEntry = null;
      let latestDate = null;
  
      updatedDataChat.forEach((entry) => {
        entry.messages.forEach((message) => {
          const messageDate = new Date(message.date);
          if (!latestDate || messageDate > latestDate) {
            latestDate = messageDate;
            latestEntry = entry;
          }
        });
      });
  
      if (latestEntry) {
        setId(latestEntry.id);
      } else {
        setId(0);
      }
    } else {
      // Initialisation de chatData s'il n'y a pas de sauvegarde
      const initializedDataChat = chatData.map((item) => ({
        ...item,
        messages: [{ role: "assistant", content: item.greeting, date: new Date().toISOString() }],
        unread: false,
      }));
      setDataChat(initializedDataChat);
      setMustSaveData(true);
      //localStorage.setItem('dataChat', JSON.stringify(initializedDataChat)); 
    }
  }, []);
  
  
  
  
  
  

  useEffect(() => {
    if (dataChat.length > 0) {
      const foundPhilosopher = dataChat.find((item) => item.id === id);
      if (foundPhilosopher) {
        if (foundPhilosopher.categorie === "Groupe") {
          setIdShowInfosWhenGroup(-1);
        }
        setPhilosopher(foundPhilosopher);
      } else {
        console.error(`Philosopher with id ${id} not found in chatData.`);
      }
    }
  }, [id, dataChat]);

  useEffect(() => {
    const unreadCount = dataChat.reduce((count, philosopher) => {
      return philosopher.unread ? count + 1 : count;
    }, 0);

    setUnreadMessages(unreadCount);
  }, [dataChat]);

  useEffect(() => {
    const handlePopState = (event) => {
      const state = event.state || {};
      if (state.isSidebarVisible !== undefined) {
        setSidebarVisible(state.isSidebarVisible);
      }
      if (state.showInfos !== undefined) {
        setShowInfos(state.showInfos);
      }
      if (state.addGroupMode !== undefined) {
        setAddGroupMode(state.addGroupMode);
      }
    };

    window.addEventListener("popstate", handlePopState);

    return () => {
      window.removeEventListener("popstate", handlePopState);
    };
  }, []);

  useEffect(() => {
    const state = {
      isSidebarVisible,
      showInfos,
      addGroupMode
    };

    const currentState = window.history.state || {};

    if (currentState.isSidebarVisible !== isSidebarVisible || currentState.showInfos !== showInfos || currentState.addGroupMode !== addGroupMode) {
      window.history.pushState(state, "");
    }
  }, [isSidebarVisible, showInfos, addGroupMode]);

  // On définit d'abord setInputUserForId
  const setInputUserForId = useCallback((value) => {
    setInputUserState((prevInputUser) => ({
      ...prevInputUser,
      [id]: value,
    }));
  }, [id]);

  // Maintenant on peut l'utiliser dans prompt()
  const prompt = useCallback((message) => {
    if (timeoutIdRef.current) {
      clearTimeout(timeoutIdRef.current);
    }
    let index = 1;

    const typeCharacter = () => {
      if (index <= message.length) {
        setInputUserForId(message.substring(0, index));
        index++;
        if (index <= message.length) {
          timeoutIdRef.current = setTimeout(typeCharacter, 8);
        }
      }
    };

    const stopGen = () => {
      if (timeoutIdRef.current) {
        clearTimeout(timeoutIdRef.current);
      }
      setInputUserForId("");
    };

    setInputUserForId("");
    typeCharacter();

    return stopGen;
  }, [setInputUserForId]);


  const updateMessages = useCallback((newMessage) => {  // fonction pour remplacer les messages du philosophe en cours
    setDataChat((prevDataChat) =>
      prevDataChat.map((philosopher) =>
        philosopher.id === id
          ? {
            ...philosopher,
            messages: newMessage
          }
          : philosopher
      )
    );
  }, [id]);

  useEffect(() => {   // enregistrement des données locales
    if (mustSaveData & dataChat.length > 0) {
      //console.log("on enregistre en local");
      localStorage.setItem('dataChat', JSON.stringify(dataChat));
      setMustSaveData(false);
    }
  }, [mustSaveData, dataChat]);

  const handlePhilosopherClick = useCallback((philosopherId) => { //quand on clique sur un philosophe dans la sideBar
    setId(philosopherId);
    setSidebarVisible(false);
  }, []);

const setOldMessagesPhilosopher = useCallback((newOldMessages) => { // la fonction à passer pour pouvoir changer directement les oldMessages sans avoir à passer l'ID
  setOldMessages((prevOldMessages) => ({
    ...prevOldMessages,
    [id]: newOldMessages || [], // Assigne directement la nouvelle valeur ou un tableau vide si la valeur est falsy
  }));
}, [id]);
  
  

  return (
    <div className="background">
      <div className="app-container">
        <div className={`left-menu ${!isSidebarVisible ? 'hidden' : ''}`}>
          <ChatSearch
            searchText={searchText}
            setSearchText={setSearchText}
            setFiltreEpoque={setFiltreEpoque}
            prompt={prompt}
            id={id}
            setId={handlePhilosopherClick}
            dataChat={dataChat}
            filtreEpoque={filtreEpoque}
            addGroupMode={addGroupMode}
            setAddGroupMode={setAddGroupMode}
            setDataChat={setDataChat}
            setSidebarVisible={setSidebarVisible}
            setLatestId={setLatestId}
            smallImages={smallImages}
            setMustSaveData={setMustSaveData}

          />
        </div>
        <div className={`right-content ${showInfos ? 'show-infos' : ''}`}>
          <div className="affichageChat">
            <ChatBox
              inputUser={inputUser[id] || ""}
              setInputUser={setInputUserForId}
              prompt={prompt}
              stopGen={() => prompt("")}
              philosopher={philosopher}
              setPhilosopher={setPhilosopher}
              messages={dataChat.find((item) => item.id === id)?.messages || []}
              setMessages={updateMessages}
              setShowInfos={setShowInfos}
              setSidebarVisible={setSidebarVisible}
              dataChat={dataChat}
              setIdShowInfosWhenGroup={setIdShowInfosWhenGroup}
              latestId={latestId}
              setId={setId}
              setDataChat={setDataChat}
              smallImages={smallImages}
              oldMessagesPhilosopher={oldMessages[id] || []}
              setOldMessagesPhilosopher={setOldMessagesPhilosopher}
              setMustSaveData={setMustSaveData}
            />
          </div>
          <InfosPhilosophe
            philosopher={philosopher?.categorie === "Groupe" && idShowInfosWhenGroup !== -1 ? dataChat.find((item) => item.id === idShowInfosWhenGroup) : philosopher}
            showInfos={showInfos}
            setShowInfos={setShowInfos}
            prompt={prompt}
            dataChat={dataChat}
            setId={setId}
            idShowInfosWhenGroup={idShowInfosWhenGroup}
            setIdShowInfosWhenGroup={setIdShowInfosWhenGroup}
            groupMode={philosopher ? (philosopher.categorie === "Groupe") : false}
            smallImages={smallImages}
          />
        </div>
      </div>
    </div>
  );
}

export default App;
