import React, { useState, useEffect } from "react";
import ReactDOM from "react-dom";
import "./InfosPhilosophe.css";
import ReactMarkdown from 'react-markdown';

const InfosPhilosophe = ({ philosopher, showInfos, setShowInfos, prompt, dataChat, setId, idShowInfosWhenGroup, setIdShowInfosWhenGroup, groupMode, smallImages }) => {
    const [currentImage, setCurrentImage] = useState(philosopher ? smallImages[philosopher.id] : '');
    const [showLargeImage, setShowLargeImage] = useState(false);

    useEffect(() => {
        if (philosopher && typeof philosopher.id === 'number' && philosopher.categorie !== "Groupe") {
            const initialImage = smallImages[philosopher.id];
            setCurrentImage(initialImage);

            import(`../profilepics/large/${philosopher.id}.png`)
                .then((module) => {
                    const largeImage = module.default;
                    const img = new Image();
                    img.src = largeImage;
                    img.onload = () => {
                        setCurrentImage(largeImage);
                    };
                })
                .catch((error) => {
                    console.error("Failed to load large image:", error);
                });
        }
    }, [philosopher, smallImages]);

    const handleClickConceptSigne = (message) => {
        if (groupMode) return null;
        prompt("Peux-tu m'expliquer ce que tu entends par " + message + " ?");
        setShowInfos(false);
    };

    const handleClickTheme = (message) => {
        if (groupMode) return null;
        prompt("Comment analyses-tu " + message + " ?");
        setShowInfos(false);
    };

    const handleImageClick = () => {
        setShowLargeImage(true);
    };

    const closeModal = () => {
        setShowLargeImage(false);
    };

    const handlePhilosopherClick = (id) => {
        setIdShowInfosWhenGroup(id);
    };


    const datesEnMinuscules = philosopher?.dates?.toLowerCase();


    if (groupMode && idShowInfosWhenGroup === -1) {
        return (
            <>
                <div className={`voletDroite ${showInfos ? 'voletActif' : 'voletInactif'}`}>
                    <div className="titreEtCroix" style={{ backgroundColor: 'white' }}>
                        <span data-icon="x" className="fermerCroix" onClick={() => setShowInfos(false)}>
                            <svg viewBox="0 0 24 24" height="24" width="24" preserveAspectRatio="xMidYMid meet" fill="currentColor">
                                <title>x</title>
                                <path d="M19.6004 17.2L14.3004 11.9L19.6004 6.60005L17.8004 4.80005L12.5004 10.2L7.20039 4.90005L5.40039 6.60005L10.7004 11.9L5.40039 17.2L7.20039 19L12.5004 13.7L17.8004 19L19.6004 17.2Z"></path>
                            </svg>
                        </span>
                        <div className="titre">Infos du groupe</div>
                    </div>
                    <div className="allInfos boxInfos" style={{alignItems:'flex-start'}}>
                        {philosopher.philosophes.map((philoId, index) => {
                            const philoInfo = dataChat.find(p => p.id === philoId);
                            const datesPhilo = philoInfo.dates.toLowerCase();
                            return (
                                <div key={index} className="philoGroupInfo" onClick={() => handlePhilosopherClick(philoInfo.id)}>
                                    <img src={smallImages[philoInfo.id]} className="imageGroup" alt={philoInfo.nom_complet} style={{ width: '100px', height: '100px' }} />
                                    <div className="infosGroupePhi">
                                        <div style={{ fontSize: '18px', fontWeight: 'bold' }}>{philoInfo.nom_complet}</div>
                                        <div style={{ fontSize: '14px', color: '#8696A0' }}>
                                            {philoInfo.lieu + (philoInfo.lieu ? ", " : "")}{datesPhilo}
                                        </div>
                                    </div>
                                </div>
                            );
                        })}
                    </div>
                </div>
            </>
        );
    }

    const filteredConcepts = philosopher?.concepts_signés?.filter(
        conceptSigné => !conceptSigné.concept.startsWith('#')
    ) || [];

    const modalContent = showLargeImage && (
        <div className={`modalOverlay ${showLargeImage ? 'show' : ''}`} onClick={closeModal}>
            <span className="closeButton" onClick={closeModal}>
                <svg viewBox="0 0 24 24" height="24" width="24" preserveAspectRatio="xMidYMid meet" fill="currentColor">
                    <title>x</title>
                    <path d="M19.6004 17.2L14.3004 11.9L19.6004 6.60005L17.8004 4.80005L12.5004 10.2L7.20039 4.90005L5.40039 6.60005L10.7004 11.9L5.40039 17.2L7.20039 19L12.5004 13.7L17.8004 19L19.6004 17.2Z"></path>
                </svg>
            </span>
            <div className="modalContent">
                <img src={currentImage} alt="Large Philosopher" className="largeImage" />
            </div>
        </div>
    );



    return (
        <>
            <div className={`voletDroite ${showInfos ? 'voletActif' : 'voletInactif'}`}>
                <div className="titreEtCroix" style={{ backgroundColor: 'white' }}>
                    <span data-icon="x" className="fermerCroix" onClick={() => setShowInfos(false)}>
                        <svg viewBox="0 0 24 24" height="24" width="24" preserveAspectRatio="xMidYMid meet" fill="currentColor">
                            <title>x</title>
                            <path d="M19.6004 17.2L14.3004 11.9L19.6004 6.60005L17.8004 4.80005L12.5004 10.2L7.20039 4.90005L5.40039 6.60005L10.7004 11.9L5.40039 17.2L7.20039 19L12.5004 13.7L17.8004 19L19.6004 17.2Z"></path>
                        </svg>
                    </span>
                    <div className="titre">Infos du contact</div>
                </div>
                {philosopher && (
                    <>
                        <div className="allInfos boxInfos">
                            <div className="imageEtNom">
                                <img src={currentImage} className="image" alt="Philosopher" onClick={handleImageClick} />
                                <span style={{ color: '#3B4154', fontSize: '24px' }}>{philosopher.nom_complet}</span>
                                <span style={{ color: '#8696A0', fontSize: '16px' }}>
                                    {philosopher.lieu + (philosopher.lieu !== "" ? ", " : "")}
                                    {datesEnMinuscules}
                                </span>
                            </div>
                {(groupMode && idShowInfosWhenGroup !== -1) && <>
                <div className="containerIcone" onClick={() => {setId(idShowInfosWhenGroup); setShowInfos(false);}}>
                    <div className="iconeMessageContainer">
                        <svg viewBox="0 0 24 24" width="19" preserveAspectRatio="xMidYMid meet" className="iconeMessage" version="1.1" x="0px" y="0px" enableBackground="new 0 0 24 24"><title>chat</title><path fill="currentColor" enableBackground="new    " d="M19.005,3.175H4.674C3.642,3.175,3,3.789,3,4.821V21.02 l3.544-3.514h12.461c1.033,0,2.064-1.06,2.064-2.093V4.821C21.068,3.789,20.037,3.175,19.005,3.175z M14.016,13.044H7.041V11.1 h6.975V13.044z M17.016,9.044H7.041V7.1h9.975V9.044z"></path></svg>
                    </div>
                    <div className="textMessage">Message</div>

                    </div>
                </>
                }
                        </div>
                        <div className="infos boxInfos">
                            <span style={{ color: '#667781', fontSize: '15px', marginBottom: '10px' }}>Infos</span>
                            <span style={{ color: '#111B21', fontSize: '15px', textAlign: 'justify' }}>
                                <ReactMarkdown>{philosopher.infos}</ReactMarkdown>
                            </span>
                        </div>
                        {(filteredConcepts.length > 0 || philosopher?.concepts.length > 0) && (
                            <div className="infos boxInfos" style={{ gap: '40px' }}>
                                {filteredConcepts.length > 0 && (
                                    <div>
                                        <div style={{ color: '#667781', fontSize: '15px', marginBottom: '10px' }}>Concepts</div>
                                        <div style={{ display: 'flex', gap: '7px 10px', flexWrap: 'wrap' }}>
                                            {filteredConcepts.map((conceptSigné, index) => (
                                                <div
                                                    onClick={() => handleClickConceptSigne(conceptSigné.concept)}
                                                    key={index}
                                                    className={`searchTag ${groupMode ? "disabled" : "gris"}`}
                                                >
                                                    {conceptSigné.concept}
                                                </div>
                                            ))}
                                        </div>
                                    </div>
                                )}
                                {philosopher?.concepts.length > 0 && (
                                    <div>
                                        <div style={{ color: '#667781', fontSize: '15px', marginBottom: '10px' }}>Thèmes abordés</div>
                                        <div style={{ display: 'flex', gap: '7px 10px', flexWrap: 'wrap' }}>
                                            {philosopher?.concepts.map((theme, index) => (
                                                <div
                                                    onClick={() => handleClickTheme(theme)}
                                                    key={index}
                                                    className={`searchTag ${groupMode ? "disabled" : "gris"}`}
                                                >
                                                    {theme}
                                                </div>
                                            ))}
                                        </div>
                                    </div>
                                )}
                            </div>
                        )}
                    </>
                )}
            </div>
            {ReactDOM.createPortal(modalContent, document.body)}
        </>
    );
};

export default InfosPhilosophe;
