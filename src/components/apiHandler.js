// Il est conseillé d'inclure une fonction d'encodage, de même qu'une fonction de décodage dans proxy.php
// Dans le cas contraire, n'importe qui pourrait utiliser votre proxy.php pour communiquer gratuitement avec l'API d'OpenAI
// J'ai de mon côté une routine dans le front pour fournir un code à partir du contenu du message, que le back comparera au message
// Je ne fournis pas ce code (qui n'est pas très compliqué !), parce qu'autrement mon fichier proxy.php serait vulnérable
// Il suffit juste qu'à partir du paramètre fourni, la fonction encodeJS fournisse la même valeur que la fonction encodePHP

import { encode } from "./encoder/encodeJS";

function canonicalize(input) {
  const parseAndSort = (obj) => {
    if (obj === null || typeof obj !== 'object') {
      return obj;
    }
    if (Array.isArray(obj)) {
      return obj.map(parseAndSort);
    }
    return Object.keys(obj)
      .sort()
      .reduce((acc, key) => {
        acc[key] = parseAndSort(obj[key]);
        return acc;
      }, {});
  };

  return JSON.stringify(parseAndSort(JSON.parse(input)));
}

export const sendMessageToApi = async (messages, onResponseChunk, setThinking, setGenerating, prePrompt, specializedPrompt, keepBusy = false) => {
  let stopRequested = false;
  let receivedText = '';

  function stopAll() {
    stopRequested = true;
    setThinking(false);
    setGenerating(false);

    let finalText = receivedText.endsWith('▮') ? receivedText.slice(0, -1) : receivedText;

    if (finalText.trim() === "") {
      finalText = "...";
    }

    onResponseChunk(finalText);
  }

  setThinking(true);
  setGenerating(true);
  onResponseChunk(" ");

  try {
    const input = messages.concat([{ role: "system", content: prePrompt.avant + specializedPrompt + prePrompt.apres }]);
    const inputString = JSON.stringify(input);

    // Canonicalize JSON to a canonical form
    const canonicalInputString = canonicalize(inputString);

    // Generate a hash by encoding the canonical input string
    const secret_key = await encode(canonicalInputString);

    const response = await fetch(window.location.hostname === 'localhost'
      ? 'http://localhost:80/proxy.php'
      : 'https://philogpt.vmirebeau.fr/serveur/proxy.php', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        input: canonicalInputString,
        key: secret_key,
      })
    });

    if (!response.body) {
      throw new Error('ReadableStream n\'est pas encore supporté par ce navigateur.');
    }

    const reader = response.body.getReader();
    const decoder = new TextDecoder('utf-8');

    let done = false;
    while (!done && !stopRequested) {
      const { value, done: streamDone } = await reader.read();
      done = streamDone;

      if (value) {
        const chunk = decoder.decode(value, { stream: true });
        const jsonChunks = chunk
          .split('\n')
          .filter(line => line.startsWith('data:'))
          .map(line => line.replace('data: ', '').trim());

        for (const jsonChunk of jsonChunks) {
          if (jsonChunk === '[DONE]') {
            stopAll();
            return;
          }

          try {
            const parsedChunk = JSON.parse(jsonChunk);
            const choices = parsedChunk.choices;

            if (choices && choices.length > 0) {
              const content = choices[0].delta.content;
              if (content) {
                receivedText += content;
                onResponseChunk(receivedText + "▮");
              }
            }
          } catch (error) {
            console.error('Erreur pendant le traitement du paquet:', error);
            continue;
          }
        }
      }
    }

    stopAll();

  } catch (error) {
    console.error('Erreur:', error);
    setThinking(false);
    setGenerating(false);
    onResponseChunk("Une erreur s'est produite. " + error);
  }

  return stopAll;
};
