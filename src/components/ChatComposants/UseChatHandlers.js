import { useCallback } from "react";
import { sendMessageToApi } from "../apiHandler";
import { getInstructions, getInstructionsGroupe, getSpecializedPrompt } from "../../consts/prompts";


export const useChatHandlers = ({
  messages, // l'ensemble des messages du philosophe à afficher
  oldMessagesPhilosopher, // les "vieux" messages qui ne sont pas envoyés dans le prompt
  philosopher, // le philosophe (ou groupe)
  setMessages, // la fonction pour définir l'ensemble des messages
  setOldMessagesPhilosopher, // la fonction pour définir les vieux messages
  stopAll, 
  setStopAll,
  stopGen, // fonction pour arrêter la génération
  setThinking, // fonction pour définir le fait qu'on soit en train de réfléchir ou non (pour l'affichage du stop par exemple)
  setGenerating, // fonction pour définir le fait qu'on soit en train de générer ou non
  dataChat, // l'ensemble des données, infos et messages
  setReponsesEnCours, // fonction : est-ce qu'on a appuyé sur le bouton et on attend la fin de la génération ?
  setMustSaveData // est-ce qu'il faut enregistrer les données dans le localStorage ?
}) => {
  const handleSendMessage = useCallback(async (message) => {
    
    stopGen();

    if (stopAll) {
      setReponsesEnCours(false);
      stopAll(); // Annule le traitement en cours
    }

    const controller = new AbortController();
    const signal = controller.signal;

    const stopAllFunc = () => {
      setMustSaveData(true);
      controller.abort();
      setReponsesEnCours(false);
      setThinking(false);
      setGenerating(false);
    };

    setStopAll(() => stopAllFunc);  // Correction ici, utilisation de setStopAll

    let recentMessages = messages;
    if (messages.length > 8) {
      const oldMessagesToAdd = messages.slice(0, messages.length - 8); // Les anciens messages à déplacer
      setOldMessagesPhilosopher([...oldMessagesPhilosopher, ...oldMessagesToAdd]); // Ajoute les anciens messages à oldMessagesPhilosopher
      recentMessages = messages.slice(-8); // Garde les 8 plus récents
      setMessages(recentMessages); // Met à jour l'état avec les messages limités

    }


    const filterMessageKeys = (messages) => {
      return messages
        .filter((msg) => msg.role === "user" || msg.role === "assistant")
        .map((msg) => ({
          content: msg.content,
          date: msg.date,
          role: msg.role,
        }));
    };


    setStopAll(() => stopAllFunc);


    if (philosopher.categorie === "Groupe") {
      const idsPhilosophes = philosopher.philosophes;
      const ordreAleatoire = idsPhilosophes.slice().sort(() => Math.random() - 0.5);

      let messagesActuels = [
        ...recentMessages,
        { role: "user", content: message, date: new Date().toISOString() },
      ];
      setMessages(messagesActuels);

      setReponsesEnCours(true);

      for (let i = 0; i < ordreAleatoire.length; i++) {
        if (signal.aborted) break; // Arrête si le signal est annulé

        const idPhilosophe = ordreAleatoire[i];
        const participantActuel = dataChat.find((p) => p.id === idPhilosophe);
        const nomParticipantActuel = participantActuel ? participantActuel.nom : "";

        const prompt_group_id = getInstructionsGroupe(idPhilosophe, ordreAleatoire, dataChat, participantActuel.prompt);

        // Créer une copie locale de messagesActuels à l'intérieur de la boucle
        const localMessages = [...messagesActuels];

        const filteredMessages = filterMessageKeys(localMessages);

        await sendMessageToApi(
          filteredMessages,
          (partialResponse) => {

            if (signal.aborted) return;

            // Travailler avec une copie locale
            let updatedMessages = [...localMessages];

            const lastMessage = updatedMessages[updatedMessages.length - 1];
            if (lastMessage && lastMessage.role === "assistant" && lastMessage.name === nomParticipantActuel) {
              updatedMessages[updatedMessages.length - 1] = {
                role: "assistant",
                content: partialResponse,
                date: new Date().toISOString(),
                name: nomParticipantActuel,
                imgId: idPhilosophe,
              };
            } else {
              updatedMessages.push({
                role: "assistant",
                content: partialResponse,
                date: new Date().toISOString(),
                name: nomParticipantActuel,
                imgId: idPhilosophe,
              });
            }

            // Réassigner localMessages à updatedMessages pour la prochaine itération
            messagesActuels = [...updatedMessages];
            setMessages(updatedMessages);
          },
          setThinking,
          setGenerating,
          prompt_group_id,
          getSpecializedPrompt(message, participantActuel),
          signal
        );
        setMustSaveData(true);
      }
    } else {
      const filteredMessages = filterMessageKeys(recentMessages);
      const newMessages = [
        ...filteredMessages,
        { role: "user", content: message, date: new Date().toISOString() },
      ];
      setMessages(newMessages);

      const preprompt_ = getInstructions(philosopher.nom, philosopher.prompt, philosopher.markdown);
      setReponsesEnCours(true);


      await sendMessageToApi(
        newMessages,
        (partialResponse) => {

          if (signal.aborted) return;

          let updatedMessages = [...newMessages];

          const lastMessage = updatedMessages[updatedMessages.length - 1];
          if (lastMessage && lastMessage.role === "assistant") {
            updatedMessages[updatedMessages.length - 1] = {
              role: "assistant",
              content: partialResponse,
              date: new Date().toISOString(),
            };
          } else {
            updatedMessages.push({
              role: "assistant",
              content: partialResponse,
              date: new Date().toISOString(),
            });
          }

          setMessages(updatedMessages);
        },
        setThinking,
        setGenerating,
        preprompt_,
        getSpecializedPrompt(message, philosopher),
        signal
      );
      setMustSaveData(true);
    }
    setReponsesEnCours(false); // on n'est plus en phase de génération

  }, [
    messages,
    oldMessagesPhilosopher,
    philosopher,
    setMessages,
    setOldMessagesPhilosopher,
    stopAll,
    stopGen,
    dataChat,
    setThinking,
    setGenerating,
    setStopAll,
    setMustSaveData,
    setReponsesEnCours
  ]);

  return { handleSendMessage };
};
