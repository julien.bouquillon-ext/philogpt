import React, { useMemo, useEffect, useRef, useCallback } from 'react';
import Message from './Message';

function MessageList({
  messages, // l'ensemble des messages du philosophe à afficher
  oldMessagesPhilosopher, // les "vieux" messages qui ne sont pas envoyés dans le prompt
  philosopher,  // le philosophe (ou groupe)
  smallImages,  // le tableau des petites images
  handlePhilosopherClick, // fonction qui affiche les infos dans le volet de droite
  thinking, // est-ce qu'on est en train de réfléchir ? (= initialisation + générationZ)
  setFirstAssistantTop,
  reponsesEnCours // est-ce qu'on a cliqué sur le bouton d'envoi et qu'une génération est en cours ?
}) {
  const combinedMessages = useMemo(() => {
    return (oldMessagesPhilosopher || []).concat(messages);
  }, [messages, oldMessagesPhilosopher]);

  const refs = useRef([]); // Crée un tableau de refs pour chaque message-container
  const messageRefs = useRef([]); // Crée un tableau de refs pour chaque div "message"

  // Fonction pour trouver le bon message assistant
  const findRelevantAssistantMessage = useCallback(() => {
    for (let i = combinedMessages.length - 1; i >= 0; i--) {
      const msg = combinedMessages[i];
      if (msg.role !== 'assistant') {
        if (i + 1 < combinedMessages.length && messageRefs.current[i + 1]) {
          return messageRefs.current[i + 1]; // Retourne la ref du premier message assistant après ce message
        }
        break;
      }
    }
    return refs.current[0] || null;
  }, [combinedMessages]);

  // Fonction pour récupérer la position du message assistant trouvé et inclure le margin-top de "message"
  const getFirstAssistantTop = useCallback(() => {
    const relevantMessageRef = findRelevantAssistantMessage();
    if (relevantMessageRef) {
      const messageOffsetTop = relevantMessageRef.offsetTop; // Utilise offsetTop pour récupérer la position relative au conteneur parent
      //console.log("getFirstAssistantTop");
      setFirstAssistantTop(messageOffsetTop); // Met à jour la distance avec la marge incluse
    }
  }, [findRelevantAssistantMessage, setFirstAssistantTop]);

  useEffect(() => {
    if (reponsesEnCours) {
    getFirstAssistantTop(); // Utiliser useLayoutEffect garantit que le DOM est prêt
  }
  }, [reponsesEnCours, getFirstAssistantTop]);

  return (
    <>
      {Array.isArray(combinedMessages)
        ? combinedMessages.map((msg, index) => (
            <div
              key={index}
              ref={(el) => (refs.current[index] = el)} // Assigne chaque message-container à une ref
              className="message-container"
            >
              <Message
                ref={(el) => (messageRefs.current[index] = el)} // Assigne chaque "message" à une ref
                msg={msg}
                philosopher={philosopher}
                smallImages={smallImages}
                handlePhilosopherClick={handlePhilosopherClick}
                thinking={thinking}
              />
            </div>
          ))
        : null}
    </>
  );
}

export default MessageList;
