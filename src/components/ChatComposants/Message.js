import React, { useCallback, forwardRef } from 'react';
import ReactMarkdown from 'react-markdown';
import LoadingDots from '../LoadingDots';
import { LeftTailSVG, RightTailSVG, DoubleCheckSVG } from './MessageSVGs'; // on déporte les SVG pour plus de lisibilité


//Affichage d'un message singulier
const Message = forwardRef(({ // on utilise forwardRef pour passer une ref à un composant fonctionnel
  msg,  //le message, avec les propriétés name, role, content, date
  philosopher,  // le philosophe (ou groupe)
  smallImages,  // le tableau des petites images
  handlePhilosopherClick, // fonction qui affiche les infos dans le volet de droite
  thinking }, // est-ce qu'on est en train de réfléchir ? (= initialisation + générationZ)
  ref) => { // la ref passée par MessageList


  const isGroupPhilosopher = philosopher && philosopher.categorie === 'Groupe';

  const memoizedHandleClick = useCallback(() => {
    if (msg.name) handlePhilosopherClick(msg.name);
  }, [msg.name, handlePhilosopherClick]);

  return (
    <div className={`message-container big${msg.role}`} >
      <div ref={ref} className={`message msg${msg.role} ${isGroupPhilosopher && msg.role === 'assistant' ? 'decalagesmall' : ''}`}>
        {msg.role === 'assistant' && msg.name && (
          <div className="philosopher-info" onClick={memoizedHandleClick}>
            <img src={smallImages[msg.imgId]} alt={msg.name} className="imagePhilosophe" />
          </div>
        )}
        <div className={msg.role}>
          {msg.role === 'assistant' && ( // si c'est un message assistant, on affiche à gauche
            <>
              <LeftTailSVG />
              <div className="nomPhilosophe" onClick={memoizedHandleClick}>
                {msg.name}
              </div>
            </>
          )}
          {msg.content?.trim() === '' || msg.content == null ? (
            thinking ? <LoadingDots /> : null
          ) : (
            <ReactMarkdown>{msg.content}</ReactMarkdown>
          )}
          {msg.role === 'user' && <RightTailSVG /> // si c'est un message user, on affiche à droite
          }
          {msg.role !== 'interface' && // par exemple, pas d'heure apparente pour le premier message de groupe
            <div className="heure">
              {msg.date ? new Date(msg.date).toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' }) : ''}
              <span aria-label=" Lu " data-icon="msg-dblcheck" className="checkread">
                <DoubleCheckSVG />
              </span>
            </div>
          }
        </div>
      </div>
    </div>
  );
});

export default React.memo(Message); // Utilisez React.memo pour éviter les re-rendus inutiles