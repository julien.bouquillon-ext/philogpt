import React, { createContext, useState, useContext } from 'react';

// Création du contexte
const NavigationHistoryContext = createContext();

// Fournisseur du contexte
export const NavigationHistoryProvider = ({ children }) => {
  const [history, setHistory] = useState([]);
  const [currentState, setCurrentState] = useState(null);

  const navigate = (newState) => {
    setHistory([...history, currentState]);
    setCurrentState(newState);
  };

  const goBack = () => {
    const newHistory = [...history];
    const previousState = newHistory.pop();
    setHistory(newHistory);
    setCurrentState(previousState);
  };

  return (
    <NavigationHistoryContext.Provider value={{ currentState, navigate, goBack }}>
      {children}
    </NavigationHistoryContext.Provider>
  );
};

// Hook pour utiliser le contexte
export const useNavigationHistory = () => useContext(NavigationHistoryContext);
