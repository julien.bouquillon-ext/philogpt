import React from 'react';
import ReactDOM from 'react-dom';
import './AboutModal.css';
import phi_img from '../profilepics/phi.svg'

const AboutModal = ({ onClose }) => {
  const handleClickOutside = (event) => {
    event.stopPropagation();
    if (event.target.className === 'about-overlay') {
      onClose();
    }
  };

  return ReactDOM.createPortal(
    <div className="about-overlay" onClick={handleClickOutside}>
      <div className="about-content" onClick={(e) => e.stopPropagation()}>
        <div className="titreabout">À propos de PhiloGPT</div>
        <div className="imgmobile mobileShow">
        <img src={phi_img} className="imgAbout" alt="logoMobile"/>
        </div>
        <div style={{display:'flex'}}>
            <div className="logoAbout largeShow" style={{marginRight:'15px'}}>
                <img src={phi_img} className="imgAbout" alt="logo"/>
            </div>
          <div className="texteAbout"><b>PhiloGPT</b> est un simulateur de dialogue qui s'appuie sur la technologie de ChatGPT pour vous permettre de discuter avec les grand.e.s philosophes du passé.<br />Bien évidemment, il ne s'agit <i>que</i> d'une simulation grossière : pour comprendre un philosophe, rien ne peut remplacer la fréquentation des textes.</div>
         
        </div>
        <div className="avertissement"><span style={{color:'red', fontWeight:700}}>Attention :</span> pour des raisons techniques, les propos générés peuvent être incorrects, et ne pas représenter la véritable position du philosophe concerné. Veillez à toujours vérifier vos informations avant d'attribuer une idée à un auteur.</div>
        <div className="about-boutonsbas">
            <a className="mailAbout" href="mailto:vmirebeau.dev@gmail.com">vmirebeau.dev@gmail.com</a>
          <div className="about-button" onClick={onClose}>Fermer</div>
        </div>
      </div>
    </div>,
    document.body
  );
};

export default AboutModal;
