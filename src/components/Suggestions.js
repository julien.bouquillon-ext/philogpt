import React, { useState, useEffect, useRef } from "react";
import "./Suggestions.css";

const Suggestions = ({ prompt, philosopher, dataChat }) => {
  const [suggestionIndices, setSuggestionIndices] = useState([]);
  const previousPhilosopherId = useRef(null);

  function capitalize(text) {
    if (typeof text !== 'string' || text.length === 0) {
      return '';
    }
    return text.charAt(0).toUpperCase() + text.slice(1);
  }

  useEffect(() => {
    if (philosopher && philosopher.id !== previousPhilosopherId.current) {
      previousPhilosopherId.current = philosopher.id;

      let suggestionsArray = [];
      if (philosopher.categorie === "Groupe") {
        philosopher.philosophes.forEach((id) => {
          const foundPhilosopher = dataChat.find((x) => x.id === id);
          if (foundPhilosopher && Array.isArray(foundPhilosopher.concepts)) {
            suggestionsArray.push(...foundPhilosopher.concepts);
          }
        });

        // On enlève les doublons et on filtre les entrées invalides
        suggestionsArray = [...new Set(suggestionsArray)].filter(Boolean);
      } else if (Array.isArray(philosopher.suggestions)) {
        suggestionsArray = philosopher.suggestions.map(suggestion => suggestion.title);
      }

      if (suggestionsArray.length > 0) {
        // On s'assure qu'il y a des suggestions entre lesquelles choisir (sinon ça plante)
        const initialIndices = [];
        while (initialIndices.length < 3 && suggestionsArray.length > 0) {
          const randomIndex = Math.floor(Math.random() * suggestionsArray.length);
          if (!initialIndices.includes(randomIndex)) {
            initialIndices.push(randomIndex);
          }
        }
        setSuggestionIndices(initialIndices);
      } else {
        setSuggestionIndices([]); // On vide s'il n'y a pas de suggestions
      }
    }
  }, [philosopher, dataChat]);

  const handleSuggestionClick = (clickedIndex) => {
    let suggestionsArray =
      philosopher.categorie === "Groupe"
        ? philosopher.philosophes.reduce((acc, id) => {
            const foundPhilosopher = dataChat.find(philo => philo.id === id);
            if (foundPhilosopher && Array.isArray(foundPhilosopher.concepts)) {
              return acc.concat(foundPhilosopher.concepts);
            }
            return acc;
          }, [])
        : philosopher.suggestions.map(suggestion => suggestion.title);

    suggestionsArray = [...new Set(suggestionsArray)].filter(Boolean);

    if (suggestionsArray.length > 0 && suggestionIndices[clickedIndex] < suggestionsArray.length) {
      const selectedSuggestion = suggestionsArray[suggestionIndices[clickedIndex]];
      if (selectedSuggestion) {
        const newPrompt =
          philosopher.categorie === "Groupe"
            ? `Comment comprenez-vous ${selectedSuggestion} ? Quels sont vos points d'accord et de désaccord ?`
            : philosopher.suggestions[suggestionIndices[clickedIndex]].prompt;

        prompt(newPrompt);

        const newIndices = [...suggestionIndices];
        let newRandomIndex;
        let tries = 0;
        do {
          newRandomIndex = Math.floor(Math.random() * suggestionsArray.length);
          tries++;
        } while (newIndices.includes(newRandomIndex) && suggestionsArray.length > 1 && tries < 10);

        newIndices[clickedIndex] = newRandomIndex;

        const suggestionElement = document.querySelectorAll('.suggestion-btn')[clickedIndex];
        suggestionElement.classList.add('active');

        setTimeout(() => {
          suggestionElement.classList.remove('active');
          setSuggestionIndices(newIndices);
        }, 400); // Durée de l'animation css
      }
    }
  };

  if (!philosopher || !Array.isArray(suggestionIndices) || suggestionIndices.length === 0) {
    return null;
  }

  return (
    <div className="suggestions">
      {suggestionIndices.map((suggestionIndex, index) => {
        const suggestionsArray =
          philosopher.categorie === "Groupe"
            ? philosopher.philosophes.reduce((acc, id) => {
                const foundPhilosopher = dataChat.find(philo => philo.id === id);
                if (foundPhilosopher && Array.isArray(foundPhilosopher.concepts)) {
                  return acc.concat(foundPhilosopher.concepts);
                }
                return acc;
              }, [])
            : philosopher.suggestions.map(suggestion => suggestion.title);

        // On enlève les doublons et on filtre les mauvaises entrées
        const uniqueSuggestionsArray = [...new Set(suggestionsArray)].filter(Boolean);

        // On s'assure que suggestionIndex soit valide
        if (suggestionIndex >= uniqueSuggestionsArray.length) return null;

        const suggestion = uniqueSuggestionsArray[suggestionIndex];
        const title = capitalize(suggestion || "Suggestion");

        return (
          <div className="suggestion_container">
          <div
            key={index}
            className="suggestion-btn"
            onClick={() => handleSuggestionClick(index)}
          >
            {title}
          </div>
          </div>
        );
      })}
    </div>
  );
};

export default Suggestions;
