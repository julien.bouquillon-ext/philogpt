<?php

// Charger les variables d'environnement depuis le fichier .env
// Il y a peut-être une façon plus propre de charger de "vrais" .env, mais ça ne semble pas supporté par mon hébergement perso
// J'en passe donc par un bête chargement de fichier
include('loadenv.php');
$env = parseEnv('.env');
$api_key = $env['OPENAI_API_KEY'];

if (!$api_key) {
    http_response_code(500);
    echo json_encode(["error" => "Clé API non trouvée."]);
    exit;
}

if (function_exists('apache_setenv')) {
    apache_setenv('no-gzip', '1');
}
ini_set('zlib.output_compression', '0');

ob_implicit_flush(true);
ob_end_flush();

// Inclure la fonction d'encodage
include('encoder/encodePHP.php');

$allowed_origins = array(
    'https://philogpt.vmirebeau.fr',
    'http://localhost:3000',
    'http://localhost'
);

$origin = $_SERVER['HTTP_ORIGIN'] ?? '';

if (in_array($origin, $allowed_origins)) {
    header("Access-Control-Allow-Origin: " . $origin);
} else {
    header("Access-Control-Allow-Origin: null");
}

header('Content-Type: application/json');
header('Cache-Control: no-cache');

header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type, Authorization");

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    exit(0);
}

// Récupérer les données JSON
$json_data = file_get_contents('php://input');
$data = json_decode($json_data, true);

// Vérifier si les clés 'input' et 'key' existent dans les données JSON
if (!isset($data['input']) || !isset($data['key'])) {
    http_response_code(400);
    echo json_encode(["error" => "Champ 'input' ou 'key' manquant dans les données JSON."]);
    exit;
}

// Récupérer et traiter l'input normalisé et la clé du client
$received_json = $data['input'];

// Encoder le $received_json avec la fonction PHP encode()
$server_key = encode($received_json);

// Utiliser la clé fournie par le client
$client_key = $data['key'];

if ($server_key !== $client_key) {
    http_response_code(400);
    echo json_encode([
        "error" => "Le paramètre key ne correspond pas.",
    ]);
    exit;
}

// Si les hash correspondent, continuer le traitement avec l'API d'OpenAI
$data = array(
    'model' => 'gpt-4o-mini',
    'stream' => true,
    'messages' => json_decode($received_json, true)
);

$options = array(
    CURLOPT_URL => 'https://api.openai.com/v1/chat/completions',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_POSTFIELDS => json_encode($data),
    CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json',
        'Authorization: Bearer ' . $api_key
    ),
    CURLOPT_WRITEFUNCTION => function($ch, $chunk) {
        echo $chunk;
        return strlen($chunk);
    }
);

$ch = curl_init();
curl_setopt_array($ch, $options);
curl_exec($ch);
curl_close($ch);

?>
