<?php

// Clé API OpenAI
$OPENAI_API_KEY = "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";

// Récupération des paramètres POST
$data = json_decode(file_get_contents('php://input'), true);
$thread = isset($data['thread']) ? $data['thread'] : null;
$message = isset($data['message']) ? $data['message'] : null;

// Vérification du paramètre "message"
if ($message === null || trim($message) === '') {
    http_response_code(400);
    echo json_encode(["error" => "Le paramètre 'message' est requis."]);
    exit();
}

// Si le thread est nul, on crée un nouveau thread
if ($thread === null || trim($thread) === '') {
    $thread = create_new_thread($OPENAI_API_KEY);
    if (!$thread) {
        http_response_code(500);
        echo json_encode(["error" => "Erreur lors de la création du thread."]);
        exit();
    }
}

// Envoi du message à l'API OpenAI
$message_id = send_message_to_thread($OPENAI_API_KEY, $thread, $message);
if (!$message_id) {
    http_response_code(500);
    echo json_encode(["error" => "Erreur lors de l'envoi du message."]);
    exit();
}

// Exécution des instructions de Platon
$run_id = execute_instructions($OPENAI_API_KEY, $thread);
if (!$run_id) {
    http_response_code(500);
    echo json_encode(["error" => "Erreur lors de l'exécution des instructions."]);
    exit();
}

// Attendre que le run soit complété avec un délai minimal
if (!check_run_status($OPENAI_API_KEY, $thread, $run_id)) {
    http_response_code(500);
    echo json_encode(["error" => "Le run n'a pas pu être complété dans le temps imparti."]);
    exit();
}

// Récupération des messages du thread
$response_message = get_assistant_message($OPENAI_API_KEY, $thread);
if (!$response_message) {
    http_response_code(500);
    echo json_encode(["error" => "Erreur lors de la récupération des messages."]);
    exit();
}

// Création de la réponse finale
$response = [
    "thread" => $thread,
    "message" => $response_message
];

// Envoi de la réponse
echo json_encode($response);
exit();

function make_post_request($url, $headers, $data = null) {
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST"); // Assurez-vous que la méthode est POST
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    if ($data) {
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data)); // Ajoute les données en POST
    }

    $response = curl_exec($ch);
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    if (curl_errno($ch)) {
        error_log("cURL error: " . curl_error($ch));
        curl_close($ch);
        return null;
    }

    curl_close($ch);

    if ($httpcode == 200) {
        return json_decode($response);
    } else {
        error_log("HTTP error: $httpcode - Response: $response");
        return null;
    }
}

function create_new_thread($api_key) {
    $url = "https://api.openai.com/v1/threads";
    $headers = [
        "Content-Type: application/json",
        "Authorization: Bearer $api_key",
        "OpenAI-Beta: assistants=v2"
    ];

    // Appel de la fonction make_post_request pour créer un nouveau thread
    $response = make_post_request($url, $headers);

    if ($response) {
        if (isset($response->id)) {
            return $response->id;
        } else {
            // Journaliser la réponse d'erreur
            error_log("Erreur lors de la création du thread : " . json_encode($response));
        }
    } else {
        // Journaliser si aucune réponse n'a été reçue
        error_log("Aucune réponse reçue lors de la création du thread.");
    }

    return null;
}


// Fonction pour envoyer un message à un thread
function send_message_to_thread($api_key, $thread, $message) {
    $url = "https://api.openai.com/v1/threads/$thread/messages";
    $headers = [
        "Content-Type: application/json",
        "Authorization: Bearer $api_key",
        "OpenAI-Beta: assistants=v2"
    ];
    $data = [
        "role" => "user",
        "content" => $message
    ];

    $response = make_post_request($url, $headers, $data);
    if ($response && isset($response->id)) {
        return $response->id;
    }
    return null;
}

// Fonction pour exécuter les instructions de Platon
function execute_instructions($api_key, $thread) {
    $url = "https://api.openai.com/v1/threads/$thread/runs";
    $headers = [
        "Content-Type: application/json",
        "Authorization: Bearer $api_key",
        "OpenAI-Beta: assistants=v2"
    ];
    $data = [
        "assistant_id" => "asst_kqqkrfa9Ib47th4pI8T9zx15",
        "instructions" => "Nous sommes dans une application de dialogue, qui permet à n'importe quel étudiant de philosophie de discuter avec un grand philosophe du passé. Tu dois incarner Platon en t'appropriant ses pensées, ses expressions, ses souvenirs, de sorte que l'interlocuteur puisse vraiment avoir l'impression de dialoguer avec le personnage que tu es."
    ];

    $response = make_post_request($url, $headers, $data);
    if ($response && isset($response->id)) {
        return $response->id;
    }
    return null;
}

// Fonction pour vérifier le statut d'un run
function check_run_status($api_key, $thread, $run_id) {
    $url = "https://api.openai.com/v1/threads/$thread/runs/$run_id";
    $headers = [
        "Content-Type: application/json",
        "Authorization: Bearer $api_key",
        "OpenAI-Beta: assistants=v2"
    ];

    // Délai initial
    $delay = 0.1; // 100 ms

    // Temps maximum d'attente (en secondes)
    $timeout = 10;
    $start_time = microtime(true);

    while (microtime(true) - $start_time < $timeout) {
        $response = make_get_request($url, $headers);
        if ($response && isset($response->status) && $response->status === "completed") {
            return true;
        }
        usleep($delay * 1000000); // Délai en microsecondes
    }
    return false;
}

// Fonction pour récupérer les messages du thread et trouver la réponse de l'assistant
function get_assistant_message($api_key, $thread) {
    $url = "https://api.openai.com/v1/threads/$thread/messages";
    $headers = [
        "Content-Type: application/json",
        "Authorization: Bearer $api_key",
        "OpenAI-Beta: assistants=v2"
    ];

    $response = make_get_request($url, $headers);
    if ($response && isset($response->data)) {
        // Parcourir les messages dans l'ordre normal (du plus ancien au plus récent)
        foreach ($response->data as $message) {
            if ($message->role === "assistant" && isset($message->content[0]->text->value)) {
                return $message->content[0]->text->value;
            }
        }
    }
    return null;
}

// Fonction pour effectuer une requête GET
function make_get_request($url, $headers) {
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $response = curl_exec($ch);
    curl_close($ch);
    return json_decode($response);
}

?>
