<?php

// OpenAI API Key
$OPENAI_API_KEY = "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";

// Set headers for SSE
header('Content-Type: text/event-stream');
header('Cache-Control: no-cache');
header('Connection: keep-alive');

// Désactive la mise en mémoire tampon de sortie
ob_end_flush();
ob_implicit_flush(true);

// Flush headers immédiatement
flush();

// Retrieve POST parameters
$data = json_decode(file_get_contents('php://input'), true);
//$thread = isset($data['thread']) ? $data['thread'] : null;
//$message = isset($data['message']) ? $data['message'] : null;
$thread="";
$message="Bonjour, comment vas-tu ?";

// Vérification du paramètre "message"
if ($message === null || trim($message) === '') {
    http_response_code(400);
    echo json_encode(["error" => "Le paramètre 'message' est requis."]);
    exit();
}

// Si le thread est nul, on crée un nouveau thread
if ($thread === null || trim($thread) === '') {
    $thread = create_new_thread($OPENAI_API_KEY);
    if (!$thread) {
        http_response_code(500);
        echo json_encode(["error" => "Erreur lors de la création du thread."]);
        exit();
    }
}

// Envoi du message à l'API OpenAI
$message_id = send_message_to_thread($OPENAI_API_KEY, $thread, $message);
if (!$message_id) {
    http_response_code(500);
    echo json_encode(["error" => "Erreur lors de l'envoi du message."]);
    exit();
}

// Exécution des instructions de Platon
$run_id = execute_instructions($OPENAI_API_KEY, $thread);
if (!$run_id) {
    http_response_code(500);
    echo json_encode(["error" => "Erreur lors de l'exécution des instructions."]);
    exit();
}

// Stream the response
stream_response($OPENAI_API_KEY, $thread, $run_id);

// Function to send SSE message
function send_sse($data) {
    echo "data: " . json_encode($data) . "\n\n";
    flush(); // Push the output to the client immediately
}

// Function to send error as SSE
function send_error($message) {
    send_sse(['error' => $message]);
}

// Function to stream the response
function stream_response($api_key, $thread, $run_id) {
    $last_message_id = null;
    $complete = false;
    $retry_count = 0;
    $max_retries = 50;  // Adjust as needed

    while (!$complete && $retry_count < $max_retries) {
        $run_status = check_run_status($api_key, $thread, $run_id);
        
        if ($run_status === 'completed') {
            $complete = true;
        } elseif ($run_status === false) {
            $retry_count++;
            sleep(1);
            continue;
        }

        $new_content = get_new_content($api_key, $thread, $last_message_id);
        
        if ($new_content) {
            send_sse(['content' => $new_content]);
            $last_message_id = $new_content['id'];
        }

        if (!$complete) {
            sleep(1);  // Wait for 1 second before checking again
        }
    }

    if ($complete) {
        send_sse(['status' => 'complete']);
    } else {
        send_error("The run could not be completed within the allotted time.");
    }
}

// Function to check the status of a run
function check_run_status($api_key, $thread, $run_id) {
    $url = "https://api.openai.com/v1/threads/$thread/runs/$run_id";
    $headers = [
        "Authorization: Bearer $api_key",
        "OpenAI-Beta: assistants=v2"
    ];

    $response = make_get_request($url, $headers);
    if ($response && isset($response->status)) {
        return $response->status;
    }
    return false;
}

// Function to get new content
function get_new_content($api_key, $thread, $last_message_id) {
    $url = "https://api.openai.com/v1/threads/$thread/messages";
    $headers = [
        "Authorization: Bearer $api_key",
        "OpenAI-Beta: assistants=v2"
    ];

    $response = make_get_request($url, $headers);
    if ($response && isset($response->data)) {
        foreach ($response->data as $message) {
            if ($message->role === "assistant" && count($message->content) > 0 && (!$last_message_id || $message->id !== $last_message_id)) {
                return [
                    'id' => $message->id,
                    'text' => $message->content[0]->text->value
                ];
            }
        }
    }
    return null;
}

// Function to create a new thread
function create_new_thread($api_key) {
    $url = "https://api.openai.com/v1/threads";
    $headers = [
        "Content-Type: application/json",
        "Authorization: Bearer $api_key",
        "OpenAI-Beta: assistants=v2"
    ];

    $response = make_post_request($url, $headers);

    if ($response) {
        if (isset($response->id)) {
            return $response->id;
        } else {
            error_log("Erreur lors de la création du thread : " . json_encode($response));
        }
    } else {
        error_log("Aucune réponse reçue lors de la création du thread.");
    }

    return null;
}

// Function to send a message to a thread
function send_message_to_thread($api_key, $thread, $message) {
    $url = "https://api.openai.com/v1/threads/$thread/messages";
    $headers = [
        "Content-Type: application/json",
        "Authorization: Bearer $api_key",
        "OpenAI-Beta: assistants=v2"
    ];
    $data = [
        "role" => "user",
        "content" => $message
    ];

    $response = make_post_request($url, $headers, $data);
    if ($response && isset($response->id)) {
        return $response->id;
    }
    return null;
}

// Function to execute instructions
function execute_instructions($api_key, $thread) {
    $url = "https://api.openai.com/v1/threads/$thread/runs";
    $headers = [
        "Content-Type: application/json",
        "Authorization: Bearer $api_key",
        "OpenAI-Beta: assistants=v2"
    ];
    $data = [
        "assistant_id" => "asst_kqqkrfa9Ib47th4pI8T9zx15",
        "instructions" => "Nous sommes dans une application de dialogue, qui permet à n'importe quel étudiant de philosophie de discuter avec un grand philosophe du passé. Tu dois incarner Platon en t'appropriant ses pensées, ses expressions, ses souvenirs, de sorte que l'interlocuteur puisse vraiment avoir l'impression de dialoguer avec le personnage que tu es."
    ];

    $response = make_post_request($url, $headers, $data);
    if ($response && isset($response->id)) {
        return $response->id;
    }
    return null;
}

// Function to make a POST request
function make_post_request($url, $headers, $data = null) {
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    if ($data) {
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
    }

    $response = curl_exec($ch);
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    if (curl_errno($ch)) {
        error_log("cURL error: " . curl_error($ch));
        curl_close($ch);
        return null;
    }

    curl_close($ch);

    if ($httpcode == 200) {
        return json_decode($response);
    } else {
        error_log("HTTP error: $httpcode - Response: $response");
        return null;
    }
}

// Function to make a GET request
function make_get_request($url, $headers) {
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $response = curl_exec($ch);
    curl_close($ch);
    return json_decode($response);
}

?>
