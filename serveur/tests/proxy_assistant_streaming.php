<?php

// Clé API OpenAI
$OPENAI_API_KEY = "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";

// Récupération des paramètres POST
$data = json_decode(file_get_contents('php://input'), true);
$thread = isset($data['thread']) ? $data['thread'] : null;
$message = isset($data['message']) ? $data['message'] : null;

// Log initial des paramètres reçus
error_log("Paramètres reçus : thread = " . json_encode($thread) . ", message = " . json_encode($message));

// Vérification du paramètre "message"
if ($message === null || trim($message) === '') {
    http_response_code(400);
    echo json_encode(["error" => "Le paramètre 'message' est requis."]);
    exit();
}

// Si le thread est nul, on crée un nouveau thread
if ($thread === null || trim($thread) === '') {
    error_log("Thread est nul, création d'un nouveau thread...");
    $thread = create_new_thread($OPENAI_API_KEY);
    if (!$thread) {
        http_response_code(500);
        echo json_encode(["error" => "Erreur lors de la création du thread."]);
        exit();
    } else {
        error_log("Nouveau thread créé : " . $thread);
    }
} else {
    error_log("Utilisation du thread existant : " . $thread);
}

// Envoi du message à l'API OpenAI
$message_id = send_message_to_thread($OPENAI_API_KEY, $thread, $message);
if (!$message_id) {
    http_response_code(500);
    echo json_encode(["error" => "Erreur lors de l'envoi du message au thread $thread."]);
    exit();
} else {
    error_log("Message envoyé avec succès, message_id : " . $message_id);
}

// Exécution des instructions de Platon
$run_id = execute_instructions($OPENAI_API_KEY, $thread);
if (!$run_id) {
    http_response_code(500);
    echo json_encode(["error" => "Erreur lors de l'exécution des instructions pour le thread $thread."]);
    exit();
} else {
    error_log("Instructions exécutées avec succès, run_id : " . $run_id);
}

// Attendre que le run soit complété avec un délai minimal
if (!check_run_status($OPENAI_API_KEY, $thread, $run_id)) {
    http_response_code(500);
    echo json_encode(["error" => "Le run n'a pas pu être complété dans le temps imparti pour le thread $thread, run $run_id."]);
    exit();
} else {
    error_log("Run complété avec succès pour le thread $thread, run_id : " . $run_id);
}

// Récupération des messages du thread
$response_message = get_assistant_message($OPENAI_API_KEY, $thread);
if (!$response_message) {
    http_response_code(500);
    echo json_encode(["error" => "Erreur lors de la récupération des messages pour le thread $thread."]);
    exit();
} else {
    error_log("Message de l'assistant récupéré avec succès pour le thread $thread : " . $response_message);
}

// Création de la réponse finale
$response = [
    "thread" => $thread,
    "message" => $response_message
];

// Envoi de la réponse
echo json_encode($response);
exit();

// Fonction pour créer un nouveau thread
function create_new_thread($api_key) {
    $url = "https://api.openai.com/v1/threads";
    $headers = [
        "Content-Type: application/json",
        "Authorization: Bearer $api_key",
        "OpenAI-Beta: assistants=v2"
    ];

    $response = make_post_request($url, $headers);
    if ($response && isset($response->id)) {
        return $response->id;
    }
    return null;
}

// Fonction pour envoyer un message à un thread
function send_message_to_thread($api_key, $thread, $message) {
    $url = "https://api.openai.com/v1/threads/$thread/messages";
    $headers = [
        "Content-Type: application/json",
        "Authorization: Bearer $api_key",
        "OpenAI-Beta: assistants=v2"
    ];
    $data = json_encode([
        "role" => "user",
        "content" => $message
    ]);

    $response = make_post_request($url, $headers, $data);
    
    // Log de la réponse brute pour débogage
    error_log("Réponse brute de l'envoi de message au thread $thread: " . json_encode($response));
    
    if ($response && isset($response->id)) {
        return $response->id;
    } else {
        error_log("Erreur lors de l'envoi du message au thread $thread : " . json_encode($response));
    }
    return null;
}

// Fonction pour exécuter les instructions de Platon
function execute_instructions($api_key, $thread) {
    $url = "https://api.openai.com/v1/threads/$thread/runs";
    $headers = [
        "Content-Type: application/json",
        "Authorization: Bearer $api_key",
        "OpenAI-Beta: assistants=v2"
    ];
    $data = json_encode([
        "assistant_id" => "asst_kqqkrfa9Ib47th4pI8T9zx15",
        "instructions" => "Nous sommes dans une application de dialogue, qui permet à n'importe quel étudiant de philosophie de discuter avec un grand philosophe du passé. Tu dois incarner Platon en t'appropriant ses pensées, ses expressions, ses souvenirs, de sorte que l'interlocuteur puisse vraiment avoir l'impression de dialoguer avec le personnage que tu es."
    ]);

    $response = make_post_request($url, $headers, $data);
    
    // Log de la réponse brute pour débogage
    error_log("Réponse brute de l'exécution des instructions pour le thread $thread: " . json_encode($response));
    
    if ($response && isset($response->id)) {
        return $response->id;
    } else {
        error_log("Erreur lors de l'exécution des instructions pour le thread $thread : " . json_encode($response));
    }
    return null;
}

// Fonction pour vérifier le statut d'un run
function check_run_status($api_key, $thread, $run_id) {
    $url = "https://api.openai.com/v1/threads/$thread/runs/$run_id";
    $headers = [
        "Content-Type: application/json",
        "Authorization: Bearer $api_key",
        "OpenAI-Beta: assistants=v2"
    ];

    // Délai initial
    $delay = 0.1; // 100 ms

    // Temps maximum d'attente (en secondes)
    $timeout = 10;
    $start_time = microtime(true);

    while (microtime(true) - $start_time < $timeout) {
        $response = make_get_request($url, $headers);
        
        // Log du statut actuel du run
        error_log("Statut actuel du run $run_id pour le thread $thread : " . json_encode($response));
        
        if ($response && isset($response->status) && $response->status === "completed") {
            return true;
        }
        usleep($delay * 1000000); // Délai en microsecondes
    }
    return false;
}

// Fonction pour récupérer les messages du thread et trouver la réponse de l'assistant
function get_assistant_message($api_key, $thread) {
    $url = "https://api.openai.com/v1/threads/$thread/messages";
    $headers = [
        "Content-Type: application/json",
        "Authorization: Bearer $api_key",
        "OpenAI-Beta: assistants=v2"
    ];

    $response = make_get_request($url, $headers);
    
    // Log de la réponse brute pour débogage
    error_log("Réponse brute de la récupération des messages pour le thread $thread: " . json_encode($response));
    
    if ($response && isset($response->data)) {
        // Parcourir les messages dans l'ordre normal (du plus ancien au plus récent)
        foreach ($response->data as $message) {
            if ($message->role === "assistant" && isset($message->content[0]->text->value)) {
                return $message->content[0]->text->value;
            }
        }
    } else {
        error_log("Erreur lors de la récupération des messages pour le thread $thread : " . json_encode($response));
    }
    return null;
}

// Fonction pour effectuer une requête POST
function make_post_request($url, $headers, $data = null) {
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    if ($data) {
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    }
    $response = curl_exec($ch);
    curl_close($ch);
    return json_decode($response);
}

// Fonction pour effectuer une requête GET
function make_get_request($url, $headers) {
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $response = curl_exec($ch);

    // Vérification des erreurs cURL
    if (curl_errno($ch)) {
        error_log("Erreur cURL GET: " . curl_error($ch));
        curl_close($ch);
        return null;
    }

    curl_close($ch);
    return json_decode($response);
}

?>
